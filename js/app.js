// function showHidedescription()
// {
//     $(this).find(".description").toggle();
// }


// $(document).ready(function(){

//     // Debut de votre code


//     $('.pizza-type label').hover(
//         showHidedescription,
//         showHidedescription

//     );
// });

$(document).ready(function () {


    var prix;


    $('.pizza-type label').hover(
        function () {
            $(this).find(".description").show();
        },
        function () {
            $(this).find(".description").hide();
        },
    );



    //fonction pour le bouton done
    $('.done').click(function () {
        //on recuper ele nom du client
        var nom = $(".infos-client .type:first input").val();
        //on vide le main et on affceh la phrase.
        $('.main').empty()
            .append("<p> Merci Monsieur " + nom + " votre commande sera la dans 15 min</p>");
    })


    //calcul du prix 
    function calculprix() {
        //valeur pour stocker le prix 
        let total = 0;

        //on ajoute a total le prix de la pate et de la pizza en fonction de ce qui a été coché
        total += +$("input[name='type']:checked").data("price") || 0;


        total += +$("input[name='pate']:checked").data("price") || 0;

        //on parcours les différents extras qui ont été coché pour savoir quel prix ajouter
        $("input[name='extra']:checked").each(function (i, elem) {
            //on ajoute la valeur des prix de chacun a notre total
            total += $(elem).data("price");
        });

        //on divise notre prix par 6 car on affiche le prix pour un part 
        total *= $(".nb-parts input").val() / 6;

        //on met a jour le texte avce notre prix 
        $(".stick-right p").text(total);
    }

    //on appelle la fonction
    $("input[data-price]").change(calculprix);

    
    



    $('.nb-parts input').on('keyup', function () {
        $(".pizza-pict").remove();
        var pizza = $('<span class="pizza-pict"></span>');
        slices = +$(this).val();

        for (i = 0; i < slices / 6; i++) {
            $(this).after(pizza.clone().addClass('pizza-6'));
        }

        if (slices % 6 != 0) $('.pizza-pict').last().removeClass('pizza-6').addClass('pizza-' + slices % 6);
    });


    $(".next-step").click(function () {
        $(this).remove();
        $(".infos-client").slideDown();
    });


    $(".add").click(function () {
        $(".infos-client .type:nth-child(2) input").append("<input type='text'/>");
        $(".infos-client .add").before("<input type='text'/>");
    });










});


